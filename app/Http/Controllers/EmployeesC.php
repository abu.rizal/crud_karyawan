<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeesC extends Controller
{
    public function data()
    {
        $karyawan = DB::table('karyawan')->get();

        return view('home', ['karyawan' => $karyawan]);
    }

    public function add()
    {
        return view('add');
    }

    public function addProcess(Request $request)
    {
        DB::table('karyawan')->insert([
            'nama_karyawan' => $request -> nama_karyawan,
            'no_karyawan' => $request -> no_karyawan,
            'no_telp_karyawan' => $request -> no_telp_karyawan,
            'jabatan_karyawan' => $request -> jabatan_karyawan,
            'divisi_karyawan' => $request -> divisi_karyawan,
        ]);
        return redirect('/')->with('status','Data berhasil ditambahkan!');
    }

    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->where('id', $id)->first();
        return view('edit', ['karyawan' => $karyawan]);
    }

    public function editProcess(Request $request, $id)
    {
        $request = DB::table('karyawan')
              ->where('id', $id)
              ->update([
                    'nama_karyawan' => $request -> nama_karyawan,
                    'no_karyawan' => $request -> no_karyawan,
                    'no_telp_karyawan' => $request -> no_telp_karyawan,
                    'jabatan_karyawan' => $request -> jabatan_karyawan,
                    'divisi_karyawan' => $request -> divisi_karyawan,
                ]);
                return redirect('/')->with('status','Data berhasil diupdate!');    }

    public function delete($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/')->with('status','Data berhasil dihapus!');    }
}
