<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CRUD Karyawan</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="{{ asset("/css/normalize.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/themify-icons.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/flag-icon.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/css/cs-skin-elastic.css") }}">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{ asset("/scss/style.css") }}">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <script src="https://kit.fontawesome.com/341dfafdd2.js" crossorigin="anonymous"></script>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>


        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./">Dashboard</a>
                <a class="navbar-brand hidden" href="./">D</a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <h3 class="menu-title">Karyawan</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Karyawan</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-user-circle"></i><a href="{{ url("/") }}">Data Karyawan</a></li>
                        </ul>
                    </li>
                    

                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                                <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                    

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Karyawan</h1>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="content mt-3">
            
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <strong>Tambah Data Karyawan</strong>
                    </div>
                    <div class="float-right">
                        <a href="{{ url('/') }}" class="btn btn-secondary btn-sm">
                            <i class="fas fa-undo"></i>Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-4 offset-md-4">
                            <form action="{{ url('/add') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="nama_karyawan">Nama Karyawan</label> <br>
                                    <input type="text" class="form-control" name="nama_karyawan" value=""> <br>

                                    <label for="no_karyawan">No. Karyawan</label> <br>
                                    <input type="text" class="form-control" name="no_karyawan" value=""> <br>

                                    <label for="no_telp_karyawan">No. Telepon Karyawan</label> <br>
                                    <input type="text" class="form-control" name="no_telp_karyawan" value=""> <br>
                                    
                                    <label for="jabatan_karyawan">Jabatan Karyawan</label> <br>
                                    <input type="text" class="form-control" name="jabatan_karyawan" value=""> <br>

                                    <label for="divisi_karyawan">Divisi Karyawan</label> <br>
                                    <input type="text" class="form-control" name="divisi_karyawan" value=""> <br>

                                    
                                </div>
                                <button type="submit" class="btn btn-success">submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            


        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="{{ asset("/js/vendor/jquery-2.1.4.min.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{ asset("/js/plugins.js") }}"></script>
    <script src="{{ asset("/js/main.js") }}"></script>


    <script src="{{ asset("/js/lib/chart-js/Chart.bundle.js") }}"></script>
    <script src="{{ asset("/js/dashboard.js") }}"></script>
    <script src="{{ asset("/js/widgets.js") }}"></script>
    <script src="{{ asset("/js/lib/vector-map/jquery.vmap.js") }}"></script>
    <script src="{{ asset("/js/lib/vector-map/jquery.vmap.min.js") }}"></script>
    <script src="{{ asset("/js/lib/vector-map/jquery.vmap.sampledata.js") }}"></script>
    <script src="{{ asset("/js/lib/vector-map/country/jquery.vmap.world.js") }}"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>

</body>
</html>
